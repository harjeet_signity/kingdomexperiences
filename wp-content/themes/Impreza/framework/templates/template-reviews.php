<?php 
/*
Template Name: Reviews
*/ 
/**
 * The template for displaying pages
 */
 defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
$us_layout = US_Layout::instance();
get_header();
us_load_template( 'templates/titlebar' );

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	
$args=array(
	'post_type' => 'reviews',
	'post_status' => 'publish',
	'posts_per_page' => 10,
	'paged'          => $paged
 );

$the_query = new WP_Query($args);
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">

		<main class="l-content" itemprop="mainContentOfPage">

			<?php do_action( 'us_before_page' ) ?>
			<section class="l-section wpb_row height_medium">
				<div class="l-section-h i-cf">
					<div class="review-setting">
						<?php if ( $the_query->have_posts() ) : ?>
					
							<!-- the loop -->
							<?php 
							
								while ( $the_query->have_posts() ) : $the_query->the_post(); 
							
									?><p><?php the_content(); ?></p><?php 
									?><h4> - <?php the_title(); ?></h4><?php
								endwhile;
								
								$big = 999999999; // need an unlikely integer

								echo paginate_links( array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var('paged') ),
									'total' => $the_query->max_num_pages
								) );
							?>
							
							<!-- end of the loop -->
							<?php wp_reset_postdata(); ?>
							
						<?php else : ?>
							<div class="row"><p><?php _e( 'Sorry, no posts found.' ); ?></p></div>
						<?php endif; ?>
					</div>
				</div>
			</section>			
			<?php do_action( 'us_after_page' ) ?>

		</main>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos . ' ' . us_dynamic_sidebar_id(); ?>" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">
				<?php us_dynamic_sidebar(); ?>
			</aside>
		<?php endif; ?>

	</div>
</div>

<?php get_footer() ?>
